from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import json
import pprint
import time
import requests
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup
import re

def process_browser_logs_for_network_events(logs,driver):
    for entry in logs:
        if("https://www.linkedin.com/voyager/api/voyagerSocialDashComments" in entry["message"]):
            log = json.loads(entry["message"])["message"]
            if(log["params"].get("request")):
                return log,1
        if("https://www.linkedin.com/voyager/api/graphql" in entry["message"] and "(count:" in entry["message"]):
            log = json.loads(entry["message"])["message"]
            if(log["params"].get("request") and "(count:10" in log["params"]["request"]["url"]):
                return log,2
capabilities = DesiredCapabilities.CHROME
capabilities["goog:loggingPrefs"] = {"performance": "ALL"} 
options = webdriver.ChromeOptions() 
options.add_argument(r"user-data-dir=C:/Users/medam/AppData/Local/Google/Chrome/User Data")
options.add_argument('--profile-directory=Default')
driver=webdriver.Chrome(options=options,desired_capabilities=capabilities)
with open("post_links.txt", "r") as f:
    links = f.read().splitlines()
for link in links:
    if " : Finished" in link:
        continue
    driver.get(link)

    try:
        button = WebDriverWait(driver, timeout=5).until(lambda d: d.find_element(By.CLASS_NAME,"comments-comments-list__show-previous-container"))
        ActionChains(driver).move_to_element(button).click(button.find_element(By.TAG_NAME,"button")).perform()
        time.sleep(3)
        logs = driver.get_log("performance")
        log,typeApi = process_browser_logs_for_network_events(logs,driver)
        
        url=log["params"]["request"]["url"]
        if(typeApi==1):
            original=url.rsplit("&",1)[0]
            original=original.replace("&count=10&","&count=100&")+"&start="
        else:
            original=re.sub("\(count:\d+","(count:100",url)
            original=re.sub(",start:\d+",",start:0",original)
        headers=log["params"]["request"]["headers"]
        cookies={}
        for cookie in driver.get_cookies():
            cookies[cookie["name"]]=cookie["value"]
        i=0
        comments=[]
        while True:
            if(typeApi==1):
                url=original+str(i)
            else:
                url=re.sub(",start:\d+",",start:"+str(i),original)
            x=requests.get(url,headers=headers,cookies=cookies)
            data=x.json()
            if(len(data["included"])==0):
                break
            for comment in data["included"]:
                if(comment.get("commentary")):
                    temp={}
                    temp["title"]=comment["commenter"]["title"]["text"]
                    temp["subtitle"]=comment["commenter"].get("subtitle", None)
                    temp["timestamp"]=str(comment["createdAt"])
                    temp["comment"]=comment["commentary"]["text"]
                    comments.append(temp)
            i+=100
    except TimeoutException:
        soup=BeautifulSoup(driver.page_source,"html.parser")
        comment_articles=soup.find_all("article",attrs={"class":"comments-comment-item"})
        comments=[]
        for comment in comment_articles:
            temp={}
            temp["title"]=comment.find("span",attrs={"class":"comments-post-meta__name-text"}).text.strip()
            temp["subtitle"]=comment.find("span",attrs={"class":"comments-post-meta__headline"}).text.strip()
            temp["timestamp"]=comment.find("time",attrs={"class":"comments-comment-item__timestamp"}).text.strip()
            temp["comment"]=comment.find("span",attrs={"dir":"ltr"}).text.strip()
            comments.append(temp)
    try:
        f=open("linkedin.json")
        file_data=json.load(f)
    except FileNotFoundError:
        f=open("linkedin.json","w")
        f.close()
        file_data=[]
    except json.decoder.JSONDecodeError:
        file_data=[]
    file_data.extend(comments)
    f=open("linkedin.json","w")
    json.dump(file_data,f)
    f.close()

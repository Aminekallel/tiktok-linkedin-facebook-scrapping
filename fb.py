from selenium import webdriver
from selenium.common.exceptions import ElementClickInterceptedException, StaleElementReferenceException, TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import json
from fake_useragent import UserAgent
from selenium.webdriver.chrome.options import Options
options = webdriver.ChromeOptions() 
options.add_argument("--window-size=1920,1080")
options.add_argument(r"user-data-dir=C:\Users\medam\AppData\Local\Google\Chrome\User Data")
options.add_argument('--profile-directory=Default')
driver = webdriver.Chrome(options=options)
driver.maximize_window()
driver.get("https://www.facebook.com/mosaiquefm/posts/pfbid0XTwcieRYbJg4ryHRYesvXAd7TgPS2hjwbwZKA5KUDJFM4sGQvzWYLfrXxmUiwDg9l")
while True:
    try:
        WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.XPATH, '//span/span[contains(text(),"comments")]'))).click()
    except (ElementClickInterceptedException,StaleElementReferenceException):
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        time.sleep(0.5)
    except TimeoutException:
        break
comments=driver.find_elements(By.XPATH,'//div[contains(@aria-label,"Comment by")]')
comments_list=[]
for comment in comments:
    comment_dict={}
    profile_element=comment.find_element(By.XPATH,'.//span/a/span/parent::a')
    comment_dict['name']=profile_element.text
    comment_dict['profile_link']=profile_element.get_attribute("href")
    try:
        comment_dict['comment']=comment.find_element(By.XPATH,'.//div/span/div/div[@style and @dir]').text
    except:
        comment_dict['comment']=""
    comment_dict['comment_link']=comment.find_element(By.XPATH,'.//li/a').get_attribute('href')
    comment_dict['time']=comment.find_element(By.XPATH,'.//li/a').text
    comments_list.append(comment_dict.copy())
f=open("comments.json","w",encoding='utf-8')
json.dump(comments_list,f,ensure_ascii = False)
f.close()
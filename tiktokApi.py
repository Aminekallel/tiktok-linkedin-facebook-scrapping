import requests
import streamlit as st
from bs4 import BeautifulSoup
import re
import json
headers = {
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
}
url=st.text_input("Link")
if(st.button("Submit")):
    account_link=re.search("(https://www.tiktok.com/@[\w|\d]+/*)",url)
    if(account_link):
        account_link=account_link.groups()[0]
        req=requests.get(account_link,headers=headers)
        soup=BeautifulSoup(req.text,"html.parser")
        data={}
        username=soup.find("h2",attrs={"data-e2e":"user-title"})
        st.write("Username:",username.text)
        data["username"]=username.text
        subtitle=soup.find("h1",attrs={"data-e2e":"user-subtitle"})
        st.write("Subtitle:",subtitle.text)
        data["subtitle"]=subtitle.text
        following=soup.find("strong",attrs={"title":"Following"})
        st.write("Following:",following.text)
        data["following"]=following.text
        followers=soup.find("strong",attrs={"title":"Followers"})
        st.write("Followers:",followers.text)
        data["followers"]=followers.text
        likes=soup.find("strong",attrs={"title":"Likes"})
        st.write("Likes:",likes.text)
        data["likes"]=likes.text
        try:
            f=open("tiktok.json")
            file_data=json.load(f)
        except FileNotFoundError:
            f=open("tiktok.json","w")
            f.close()
            file_data=[]
        except json.decoder.JSONDecodeError:
            file_data=[]
        file_data.append(data)
        f=open("tiktok.json","w")
        json.dump(file_data,f)
        f.close()
    else:
        st.write("Invalid Link")
    
    
